module bitbucket.org/pcas/certificate

go 1.16

require (
	bitbucket.org/pcastools/flag v0.0.19
	bitbucket.org/pcastools/version v0.0.5
)
