// Pcas-sign generates and signs SSL certificates for the PCAS infrastructure.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"io"
	"math/big"
	"os"
	"path"
	"time"
)

// writeFile writes the contents of b to the file with the given path and permissions. If the file already exists, an error will be returned.
func writeFile(b io.Reader, path string, perm os.FileMode) (err error) {
	var f io.WriteCloser
	// Create the file, returning an error if it exists
	f, err = os.OpenFile(path, os.O_CREATE|os.O_EXCL|os.O_WRONLY, perm)
	if err != nil {
		return
	}
	// Defer closing the file, capturing any error
	defer func() {
		closeErr := f.Close()
		if err == nil {
			err = closeErr
		}
	}()
	// Write the data
	_, err = io.Copy(f, b)
	return
}

// randomSerialNumber returns a random serial number in [0,2^160).
func randomSerialNumber() (*big.Int, error) {
	var max big.Int
	max.Exp(big.NewInt(2), big.NewInt(160), nil)
	return rand.Int(rand.Reader, &max)
}

// initCertificate generates a new signing certificate and writes it to the given paths.
func initCertificate(certificatePath string, privateKeyPath string) error {
	// Generate a new certificate
	ca, err := newCertificate("05aa5748b67a4704fa028d0249de2d1d")
	if err != nil {
		return err
	}
	ca.IsCA = true
	ca.KeyUsage = x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign
	ca.BasicConstraintsValid = true
	// Generate the private key
	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return err
	}
	// Generate the certificate
	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	if err != nil {
		return err
	}
	// PEM-encode and write out the private key and certificate
	if err := writePrivateKey(caPrivKey, privateKeyPath); err != nil {
		return err
	}
	if err := writeCertificate(caBytes, certificatePath); err != nil {
		return err
	}
	return nil
}

// readPrivateKey reads a single PEM-encoded private key from the file with the given path.
func readPrivateKey(path string) (*rsa.PrivateKey, error) {
	// Read the file contents
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	// Decode the contents
	p, _ := pem.Decode(b)
	if p == nil {
		return nil, errors.New("no key data found")
	}
	// Check the type
	if p.Type != "RSA PRIVATE KEY" {
		return nil, errors.New("not a private key")
	}
	// Unmarshal the PEM block
	return x509.ParsePKCS1PrivateKey(p.Bytes)
}

// writePrivateKey PEM-encodes the given private key and writes it to the file with the given path.
func writePrivateKey(privKey *rsa.PrivateKey, path string) error {
	b := new(bytes.Buffer)
	pem.Encode(b, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(privKey),
	})
	return writeFile(b, path, 0400)
}

// readCertificate reads a PEM-encoded certificate x509 certificate from the file with the given path.
func readCertificate(path string) (*x509.Certificate, error) {
	// Read the file contents
	b, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	// Decode the contents
	p, _ := pem.Decode(b)
	if p == nil {
		return nil, errors.New("no certificate data found")
	}
	// Check the type
	if p.Type != "CERTIFICATE" {
		return nil, errors.New("not a certificate")
	}
	// Unmarshal the PEM block
	return x509.ParseCertificate(p.Bytes)
}

// writeCertificate PEM-encodes the given ASN1-encoded x509 certificate and writes it to the file with the given path.
func writeCertificate(cert []byte, path string) error {
	b := new(bytes.Buffer)
	pem.Encode(b, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: cert,
	})
	return writeFile(b, path, 0444)
}

// newCertificate generates a new x509 certificate.
func newCertificate(commonname string) (*x509.Certificate, error) {
	// Make a random serial number
	n, err := randomSerialNumber()
	if err != nil {
		return nil, err
	}
	// Populate the certificate data
	return &x509.Certificate{
		SerialNumber: n,
		Subject: pkix.Name{
			Organization:       []string{"Parallel Computational Algebra System (PCAS)"},
			OrganizationalUnit: []string{"Department of Mathematics"},
			StreetAddress:      []string{"Exhibition Road"},
			Locality:           []string{"South Kensington"},
			Province:           []string{"Greater London"},
			PostalCode:         []string{"SW7 2AZ"},
			Country:            []string{"GB"},
			CommonName:         commonname,
		},
		NotBefore:   time.Now(),
		NotAfter:    time.Now().AddDate(10, 0, 0),
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature,
	}, nil
}

// random160 returns a slice of 160 random bytes
func random160() ([]byte, error) {
	b := make([]byte, 160)
	_, err := rand.Read(b)
	return b, err
}

// generateAndSign generates a certificate and private key for the given DNS names, and uses the given certificate and private key to sign it. The result is placed in outputDir.
func generateAndSign(ca *x509.Certificate, caPrivKey *rsa.PrivateKey, dnsNames []string, outputDir string) error {
	// Generate a new certificate
	cert, err := newCertificate(dnsNames[0])
	if err != nil {
		return err
	}
	cert.DNSNames = dnsNames
	b, err := random160()
	if err != nil {
		return err
	}
	cert.SubjectKeyId = b
	// Generate a private key
	certPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return err
	}
	// Sign the certificate
	certBytes, err := x509.CreateCertificate(rand.Reader, cert, ca, &certPrivKey.PublicKey, caPrivKey)
	if err != nil {
		return err
	}
	// PEM-encode and write out the private key and certificate
	if err := writePrivateKey(certPrivKey, path.Join(outputDir, "privkey.pem")); err != nil {
		return err
	}
	if err := writeCertificate(certBytes, path.Join(outputDir, "cert.pem")); err != nil {
		return err
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// main
/////////////////////////////////////////////////////////////////////////

// runMain executes the main program, returning any errors.
func runMain() error {
	// Parse the options
	opts := setOptions()
	// Handle the init task
	if opts.Init {
		return initCertificate(opts.CertificatePath, opts.PrivateKeyPath)
	}
	// Load the signing certificate and private key
	ca, err := readCertificate(opts.CertificatePath)
	if err != nil {
		return err
	}
	caPrivKey, err := readPrivateKey(opts.PrivateKeyPath)
	if err != nil {
		return err
	}
	// Run the task
	switch {
	case opts.GenerateAndSign:
		err = generateAndSign(ca, caPrivKey, opts.DNSNames, opts.OutputDir)
	}
	return err
}

// main
func main() {
	assertNoErr(runMain())
}
