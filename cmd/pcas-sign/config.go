// Config.go handles configuration and logging.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"errors"
	"fmt"
	"os"
	"path"
)

// Options describes the options.
type Options struct {
	Init            bool     // true if and only if we are generating a signing key
	GenerateAndSign bool     // true if and only if we are generating new certificates for a given host or hosts, and signing them
	DNSNames        []string // the host or hosts
	OutputDir       string   // the path to the output directory, for GenerateAndSign only
	PrivateKeyPath  string   // the path to the private key for the signing certificate
	CertificatePath string   // the path to the signing certificate
}

// Name is the name of the executable.
const Name = "pcas-sign"

// The possible commands.
const (
	cmdInit     = "init"
	cmdGenerate = "generate"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nil. If the error is non-nill then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Return the default options structure
	return &Options{
		PrivateKeyPath:  "master.key",
		CertificatePath: "master.crt",
	}
}

// validate validates the options.
func validate(opts *Options) error {
	if len(opts.CertificatePath) == 0 {
		return errors.New("invalid path to signing certificate")
	} else if len(opts.PrivateKeyPath) == 0 {
		return errors.New("invalid path to private key")
	}
	switch {
	case opts.GenerateAndSign:
		if len(opts.DNSNames) == 0 {
			return errors.New("missing DNS names")
		}
	}
	return nil
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	// Define the flags and usage message
	flag.SetGlobalHeader(fmt.Sprintf("%s creates and signs SSL certificates.\n\nUsage: %s [options] init\n       %s [options] generate [DNS names] [dir]", Name, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.String("signing-cert", &opts.CertificatePath, opts.CertificatePath, "The path to the signing certificate", ""),
		flag.String("signing-key", &opts.PrivateKeyPath, opts.PrivateKeyPath, "The path to the private key", ""),
		&version.Flag{AppName: Name},
	)
	// Parse the flags
	flag.Parse()
	// There should be at least one unparsed argument: the command to run
	n := flag.NArg()
	if n == 0 {
		return fmt.Errorf("one of '%s' or '%s' must be specified", cmdInit, cmdGenerate)
	}
	// How we proceed depends on the command
	switch flag.Arg(0) {
	case cmdInit:
		opts.Init = true
		// There should be no additional arguments
		if n > 1 {
			return errors.New("too many arguments")
		}
	case cmdGenerate:
		opts.GenerateAndSign = true
		// Set the target directory
		if n == 1 {
			return errors.New("missing certificate directory")
		}
		opts.OutputDir = path.Clean(flag.Arg(n - 1))
		// Read the DNS names
		opts.DNSNames = make([]string, 0, n-2)
		for i := 1; i < n-1; i++ {
			opts.DNSNames = append(opts.DNSNames, flag.Arg(i))
		}
	default:
		// Unknown command
		return fmt.Errorf("unknown command: '%s'", flag.Arg(0))
	}
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	return nil
}
